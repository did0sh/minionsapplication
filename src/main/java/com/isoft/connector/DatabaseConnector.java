package com.isoft.connector;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Abstract class for connecting to different databases.
 * 
 * @author Deyan Georgiev
 * @see PostgreDbConnectorImpl
 */
public class DatabaseConnector {

	private static final Properties props = new Properties();
	private static final Logger LOGGER = Logger.getLogger(DatabaseConnector.class);
	static {
		try {
			InputStream input = new FileInputStream(
					"C:\\Users\\user\\Documents\\������\\minionsapplication\\src\\main\\resources\\database.properties");
			props.load(input);
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
		}
	}
	private static final String DB_DRIVER_CLASS = (String) props.get("db.driver.class");
	private static final String DB_URL = (String) props.get("db.conn.url");
	private static final String DB_NAME = (String) props.get("db.name");
	private static final String USER_NAME = (String) props.get("db.username");
	private static final String USER_PASSWORD = (String) props.get("db.password");

	/**
	 * private constructor to prevent instantiation
	 */
	private DatabaseConnector() {
	}

	public static Connection getConnection() {
		try {
			Class.forName(DB_DRIVER_CLASS);
			Connection connection = DriverManager.getConnection(DB_URL + DB_NAME, USER_NAME, USER_PASSWORD);
			LOGGER.info("Connection to database successful!");
			return connection;
		} catch (SQLException e) {
			LOGGER.error("Connection failure. " + e.getMessage());
		} catch (ClassNotFoundException e) {
			LOGGER.error("Driver class not found. " + e.getMessage());
		}

		return null;
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			getConnection().close();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
		}
		super.finalize();
	}
}
