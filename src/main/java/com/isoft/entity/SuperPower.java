package com.isoft.entity;

/**
 * Enumeration class for the superpower table column.
 * @author Deyan Georgiev
 */
public enum SuperPower {
	invisibility, flying, healing, speed;
}
