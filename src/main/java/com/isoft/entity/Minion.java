package com.isoft.entity;

/**
 * Implementation class for the minions table in the database.
 * @author Deyan Georgiev
 */
public class Minion extends BaseEntity {
	
	private String name;
	private short age;
	private Town town;
	
	public Minion(int id, String name, short age, Town town) {
		super(id);
		this.name = name;
		this.age = age;
		this.town = town;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the age
	 */
	public short getAge() {
		return age;
	}

	/**
	 * @return the townId
	 */
	public Town getTown() {
		return town;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(short age) {
		this.age = age;
	}

	/**
	 * @param townId the townId to set
	 */
	public void setTown(Town town) {
		this.town = town;
	}

	@Override
	public String toString() {
		return "Minion: [name=" + name + ", age=" + age + ", town=" + town + "]";
	}
	
	
}
