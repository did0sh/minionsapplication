package com.isoft.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.entity.Minion;
import com.isoft.service.MinionService;
import com.isoft.service.MinionServiceImpl;

/**
 * Servlet responsible for the get & post requests
 * 
 * @author Deyan Georgiev
 */
public class MinionServlet extends HttpServlet {
	private static final long serialVersionUID = 4276880500448769299L;
	private static final String MINIONS_SERVLET = "/minions";
	private static final String ADD_MINION_SERVLET = "/addMinion";
	private static final String DELETE_MINION_SERVLET = "/deleteMinion";
	private static final String UPDATE_MINION_SERVLET = "/updateMinion";
	private static final Logger LOGGER = Logger.getLogger(MinionServlet.class);

	private static MinionService minionService = new MinionServiceImpl();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processGetRequest(req, resp, resp.getWriter());
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processPostRequest(req, resp);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processPutRequest(req, resp);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processDeleteRequest(req, resp);
	}

	private void processGetRequest(HttpServletRequest req, HttpServletResponse resp, PrintWriter pw) {
		String path = req.getServletPath();
		switch (path) {
		case MINIONS_SERVLET:
			resp.setContentType("application/json");
		    final ByteArrayOutputStream out = new ByteArrayOutputStream();
		    final ObjectMapper mapper = new ObjectMapper();
			String pathInfo = req.getPathInfo();
			if (pathInfo != null) {
				String minionId = pathInfo.split("/")[1];
				try {
					pw.print(mapper.writeValueAsString(minionService.getMinionById(Integer.parseInt(minionId))));
				} catch (NumberFormatException | JsonProcessingException e) {
					LOGGER.error(e);
				}
			} else {
				try {
					mapper.writeValue(out, minionService.getAllMinions());
					pw.print(new String(out.toByteArray()));
				} catch (IOException e) {
					LOGGER.error(e);
				}
			}
			break;
		default:
			break;
		}

	}

	private void processPostRequest(HttpServletRequest req, HttpServletResponse resp) {
		String path = req.getServletPath();
		switch (path) {
		case ADD_MINION_SERVLET:
			minionService.saveMinion();
			break;
		default:
			break;
		}
	}

	private void processPutRequest(HttpServletRequest req, HttpServletResponse resp) {
		String path = req.getServletPath();
		switch (path) {
		case UPDATE_MINION_SERVLET:
			String updateMinionId = req.getPathInfo().split("/")[1];
			Minion minion = minionService.getMinionById(Integer.parseInt(updateMinionId));
			minionService.updateMinion(minion);
			break;
		default:
			break;
		}
	}

	private void processDeleteRequest(HttpServletRequest req, HttpServletResponse resp) {
		String path = req.getServletPath();
		switch (path) {
		case DELETE_MINION_SERVLET:
			String deleteMinionId = req.getPathInfo().split("/")[1];
			minionService.deleteMinion(Integer.parseInt(deleteMinionId));
			break;
		default:
			break;
		}
	}

}
