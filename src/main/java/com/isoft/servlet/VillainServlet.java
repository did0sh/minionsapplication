package com.isoft.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.entity.Villain;
import com.isoft.service.VillainService;
import com.isoft.service.VillainServiceImpl;

/**
 * Servlet responsible for the get & post requests
 * 
 * @author Deyan Georgiev
 */
public class VillainServlet extends HttpServlet {
	private static final long serialVersionUID = -6769333909595281354L;
	private static final String VILLAINS_SERVLET = "/villains";
	private static final String ADD_VILLAIN_SERVLET = "/addVillain";
	private static final String DELETE_VILLAIN_SERVLET = "/deleteVillain";
	private static final String UPDATE_VILLAIN_SERVLET = "/updateVillain";
	private static final Logger LOGGER = Logger.getLogger(VillainServlet.class);
	
	private static final VillainService villainService = new VillainServiceImpl();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processGetRequest(req, resp, resp.getWriter());
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processPostRequest(req, resp);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processPutRequest(req, resp);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processDeleteRequest(req, resp);
	}

	private void processGetRequest(HttpServletRequest req, HttpServletResponse resp, PrintWriter pw) {
		String path = req.getServletPath();
		switch (path) {
		case VILLAINS_SERVLET:
			resp.setContentType("application/json");
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
		    final ObjectMapper mapper = new ObjectMapper();
			String pathInfo = req.getPathInfo();
			if (pathInfo != null) {
				String villainId = pathInfo.split("/")[1];
				try {
					pw.print(mapper.writeValueAsString(villainService.getVillainById(Integer.parseInt(villainId))));
				} catch (NumberFormatException | JsonProcessingException e) {
					LOGGER.error(e);
				}
			} else {
				try {
					mapper.writeValue(out, villainService.getAllVillains());
					pw.print(new String(out.toByteArray()));
				} catch (IOException e) {
					LOGGER.error(e);
				}
			}
			break;
		default:
			break;
		}
	}

	private void processPostRequest(HttpServletRequest req, HttpServletResponse resp) {
		String path = req.getServletPath();
		switch (path) {
		case ADD_VILLAIN_SERVLET:
			villainService.saveVillain();
			break;
		default:
			break;
		}
	}

	private void processPutRequest(HttpServletRequest req, HttpServletResponse resp) {
		String path = req.getServletPath();
		switch (path) {
		case UPDATE_VILLAIN_SERVLET:
			String updateVillainId = req.getPathInfo().split("/")[1];
			Villain villain = villainService.getVillainById(Integer.parseInt(updateVillainId));
			villainService.updateVillain(villain);
			break;
		default:
			break;
		}
	}

	private void processDeleteRequest(HttpServletRequest req, HttpServletResponse resp) {
		String path = req.getServletPath();
		switch (path) {
		case DELETE_VILLAIN_SERVLET:
			String deleteVillainId = req.getPathInfo().split("/")[1];
			villainService.deleteVillain(Integer.parseInt(deleteVillainId));
			break;
		default:
			break;
		}
	}

	
}
