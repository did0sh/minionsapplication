package com.isoft.service;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.isoft.dao.VillainDao;
import com.isoft.entity.Evilness;
import com.isoft.entity.SuperPower;
import com.isoft.entity.Villain;

/**
 * Service class, which main purpose is to encapsulate the logic from the villainDao.
 * @author Deyan Georgiev
 * @see VillainDao
 * @see VillainService
 */
public class VillainServiceImpl implements VillainService {
	private static final VillainDao villainDao = new VillainDao();
	private static final Random random = new Random();
	private static final Logger LOGGER = Logger.getLogger(VillainServiceImpl.class);

	@Override
	public void saveVillain() {
		villainDao.saveEntity(createRandomVillain());
	}

	@Override
	public void updateVillain(Villain villain) {
		if (villain != null) {
			villain.setName("updVillain" + villain.getId());
			villain.setEvilness(getRandomEvilness());
			villain.setSuperpower(getRandomSuperPower());
			villainDao.updateEntity(villain);
		} else {
			LOGGER.error("Villain with this id doesn`t exist!");
		}
	}

	@Override
	public void deleteVillain(int id) {
		villainDao.deleteEntity(id);
	}

	@Override
	public Villain getVillainById(int id) {
		return villainDao.getEntityById(id);
	}

	@Override
	public List<Villain> getAllVillains() {
		return villainDao.getAllEntities();
	}
	
	private Villain createRandomVillain() {
		int min = getAllVillains().size();
		int max = Integer.MAX_VALUE;
		int villainId = random.nextInt((max - min) + 1) + min;
		return new Villain(villainId, "viialin" + villainId, getRandomEvilness(), getRandomSuperPower());
	}

	private Evilness getRandomEvilness() {
		return Evilness.values()[random.nextInt(Evilness.values().length)];
	}

	private SuperPower getRandomSuperPower() {
		return SuperPower.values()[random.nextInt(SuperPower.values().length)];
	}

}
