package com.isoft.service;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.isoft.dao.TownDao;
import com.isoft.entity.Town;

/**
 * Service class, which main purpose is to encapsulate the logic from the townDao.
 * @author Deyan Georgiev
 * @see TownDao
 * @see TownService
 */
public class TownServiceImpl implements TownService {
	private static final TownDao townDao = new TownDao();
	private static final Random random = new Random();
	private static final Logger LOGGER = Logger.getLogger(TownServiceImpl.class);

	@Override
	public void saveTown() {
		townDao.saveEntity(createRandomTown());
	}

	@Override
	public void updateTown(Town town) {
		if (town != null) {
			town.setName("updTown" + town.getId());
			town.setCountry("updCountry" + town.getId());
			townDao.updateEntity(town);
		} else {
			LOGGER.error("Town with this id doesn`t exist!");
		}
	}

	@Override
	public void deleteTown(int id) {
		townDao.deleteEntity(id);
	}

	@Override
	public Town getTownById(int id) {
		return townDao.getEntityById(id);
	}

	@Override
	public List<Town> getAllTowns() {
		return townDao.getAllEntities();
	}
	
	private Town createRandomTown() {
		int min = getAllTowns().size();
		int max = Integer.MAX_VALUE;
		int townId = random.nextInt((max - min) + 1) + min;
		return new Town(townId, "town" + townId, "country" + townId);
	}
	
}
