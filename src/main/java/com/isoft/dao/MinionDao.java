package com.isoft.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.isoft.connector.DatabaseConnector;
import com.isoft.entity.Minion;
import com.isoft.entity.Town;

/**
 * Implementation class for CRUD operations referencing Minion.
 * 
 * @author Deyan Georgiev
 */
public class MinionDao implements EntityDao<Minion> {

	private static final Logger LOGGER = Logger.getLogger(MinionDao.class);
	private TownDao townDao = new TownDao();

	@Override
	public void saveEntity(Minion entity) {
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement stmt = connection
					.prepareStatement("INSERT INTO minions(id, name, age, town_id) values (?, ?, ?, ?)")) {
				stmt.setInt(1, entity.getId());
				stmt.setString(2, entity.getName());
				stmt.setShort(3, entity.getAge());

				if (entity.getTown() != null) {
					stmt.setInt(4, entity.getTown().getId());
					stmt.executeUpdate();
					LOGGER.info("Successfully saved the minion entity!");
					connection.close();
				} else {
					LOGGER.error("Could not save the minion entity. Town with this id doesn`t exist!");
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			} catch (NullPointerException npe) {
				LOGGER.error("Minion entity should not be null!");
			}
		}
	}

	@Override
	public void updateEntity(Minion entity) {
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement stmt = connection
					.prepareStatement("UPDATE minions set name=?, age=?, town_id=? WHERE id=?")) {
				stmt.setString(1, entity.getName());
				stmt.setShort(2, entity.getAge());

				if (entity.getTown() != null) {
					stmt.setInt(3, entity.getTown().getId());
					stmt.setInt(4, entity.getId());
					stmt.executeUpdate();
					LOGGER.info("Successfully updated the minion entity!");
					connection.close();
				} else {
					LOGGER.error("Could not update the minion entity. Town with this id doesn`t exist!");
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}  catch (NullPointerException npe) {
				LOGGER.error("Minion entity should not be null!");
			}
		}
	}

	@Override
	public void deleteEntity(int id) {
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement selectStmt = connection.prepareStatement("SELECT * FROM minions WHERE id=?")) {
				selectStmt.setInt(1, id);

				try (ResultSet rs = selectStmt.executeQuery()) {
					if (rs.next()) {
						try (PreparedStatement stmt = connection.prepareStatement("DELETE FROM minions WHERE id=?")) {
							stmt.setInt(1, id);
							stmt.executeUpdate();
							LOGGER.info("Successfully deleted the minion entity!");
						}
					} else {
						LOGGER.info("Minion with this id doesn`t exist!");
					}
				}
				connection.close();
			} catch (SQLException | NullPointerException e) {
				LOGGER.error(e.getMessage());
			}
		}
	}

	@Override
	public Minion getEntityById(int id) {
		int minionId;
		String minionName;
		short minionAge;
		int townId;
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM minions WHERE id=?")) {
				stmt.setInt(1, id);
				try (ResultSet rs = stmt.executeQuery()) {
					if (rs.next()) {
						minionId = rs.getInt("id");
						minionName = rs.getString("name");
						minionAge = rs.getShort("age");
						townId = rs.getInt("town_id");

						Town town = townDao.getEntityById(townId);
						LOGGER.info("Successfully returned the minion entity!");
						return new Minion(minionId, minionName, minionAge, town);

					} else {
						LOGGER.error("Minion with this id doesn`t exist!");
					}
					connection.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		}
		return null;
	}

	@Override
	public List<Minion> getAllEntities() {
		int minionId;
		String minionName;
		short minionAge;
		int townId;
		List<Minion> allMinions = new ArrayList<>();
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM minions")) {
				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						minionId = rs.getInt("id");
						minionName = rs.getString("name");
						minionAge = rs.getShort("age");
						townId = rs.getInt("town_id");

						Town town = townDao.getEntityById(townId);
						allMinions.add(new Minion(minionId, minionName, minionAge, town));
					}
					connection.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}

			if (allMinions.isEmpty()) {
				LOGGER.info("There are no entities in the database!");
			} else {
				LOGGER.info("Successfully returned all the minion entities!");
			}
		}

		return allMinions;

	}
}
