package com.isoft.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.entity.Town;
import com.isoft.service.TownService;
import com.isoft.service.TownServiceImpl;

/**
 * Servlet responsible for the get & post requests
 * 
 * @author Deyan Georgiev
 */
public class TownServlet extends HttpServlet {
	private static final long serialVersionUID = -4003812051481321098L;
	private static final String TOWNS_SERVLET = "/towns";
	private static final String ADD_TOWN_SERVLET = "/addTown";
	private static final String DELETE_TOWN_SERVLET = "/deleteTown";
	private static final String UPDATE_TOWN_SERVLET = "/updateTown";
	private static final Logger LOGGER = Logger.getLogger(TownServlet.class);

	private static final TownService townService = new TownServiceImpl();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processGetRequest(req, resp, resp.getWriter());
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processPostRequest(req, resp);
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processPutRequest(req, resp);
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processDeleteRequest(req, resp);
	}

	private void processGetRequest(HttpServletRequest req, HttpServletResponse resp, PrintWriter pw) {
		String path = req.getServletPath();
		switch (path) {
		case TOWNS_SERVLET:
			resp.setContentType("application/json");
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
		    final ObjectMapper mapper = new ObjectMapper();
			String pathInfo = req.getPathInfo();
			if (pathInfo != null) {
				String townId = pathInfo.split("/")[1];
				try {
					pw.print(mapper.writeValueAsString(townService.getTownById(Integer.parseInt(townId))));
				} catch (NumberFormatException | JsonProcessingException e) {
					LOGGER.error(e);
				}
			} else {
				try {
					mapper.writeValue(out, townService.getAllTowns());
					pw.print(new String(out.toByteArray()));
				} catch (IOException e) {
					LOGGER.error(e);
				}
			}
			break;
		default:
			break;
		}
	}

	private void processPostRequest(HttpServletRequest req, HttpServletResponse resp) {
		String path = req.getServletPath();
		switch (path) {
		case ADD_TOWN_SERVLET:
			townService.saveTown();
			break;
		default:
			break;
		}
	}

	private void processPutRequest(HttpServletRequest req, HttpServletResponse resp) {
		String path = req.getServletPath();
		switch (path) {
		case UPDATE_TOWN_SERVLET:
			String updateTownId = req.getPathInfo().split("/")[1];
			Town town = townService.getTownById(Integer.parseInt(updateTownId));
			townService.updateTown(town);
			break;
		default:
			break;
		}
	}

	private void processDeleteRequest(HttpServletRequest req, HttpServletResponse resp) {
		String path = req.getServletPath();
		switch (path) {
		case DELETE_TOWN_SERVLET:
			String deleteTownId = req.getPathInfo().split("/")[1];
			townService.deleteTown(Integer.parseInt(deleteTownId));
			break;
		default:
			break;
		}
	}
}
