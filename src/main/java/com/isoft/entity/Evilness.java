package com.isoft.entity;

/**
 * Enumeration class for the evilness table column.
 * @author Deyan Georgiev
 */
public enum Evilness {
	good, bad, evil, superevil;
}
