package com.isoft.dao;

import java.util.List;

/**
 * Generic interface for CRUD operations.
 * @author Deyan Georgiev
 */
public interface EntityDao<E> {

	/**
	 * Method for saving an entity in the database.
	 * @param entity
	 */
	void saveEntity(E entity);
	
	/**
	 * Method for updating an entity in the database.
	 * @param entity
	 */
	void updateEntity(E entity);
	
	/**
	 * Method for deleting an entity in the database.
	 * @param id
	 */
	void deleteEntity(int id);
	
	/**
	 * Method for getting an entity by its id from the database.
	 * @param id
	 * @return the entity
	 */
	E getEntityById(int id);
	
	/**
	 * Method for getting all entities from the database.
	 * @return
	 */
	List<E> getAllEntities();
}
