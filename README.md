## Minions Application ##

This is basically a simple **JAVA MAVEN** application based on
**minions** and **villains**.
**Minions** live in a town and can be servants to many villains.
On the other hand, **villains** can have many minions that can serve them.

Build tools used:

1.	Maven
2.	JDBC
3.	Servlets
4.	PostgreSQL (pgAdmin 4)
5.	Tomcat 7
6.	Java 1.8

---