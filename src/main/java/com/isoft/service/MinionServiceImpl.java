package com.isoft.service;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import com.isoft.dao.MinionDao;
import com.isoft.dao.TownDao;
import com.isoft.entity.Minion;
import com.isoft.entity.Town;

/**
 * Implementation class which main purpose is to encapsulate the logic from the MinionDao
 * @author Deyan Georgiev
 * @see MinionDao
 * @see MinionService
 */
public class MinionServiceImpl implements MinionService {
	private static final MinionDao minionDao = new MinionDao();
	private static final TownDao townDao = new TownDao();
	private static final Random random = new Random();
	private static final Logger LOGGER = Logger.getLogger(MinionServiceImpl.class);

	@Override
	public void saveMinion() {
		minionDao.saveEntity(createRandomMinion());
	}

	@Override
	public void updateMinion(Minion minion) {
		if (minion != null) {
			minion.setName("updMinion" + minion.getId());
			minion.setAge(getRandomAge());
			minion.setTown(getRandomTown());
			minionDao.updateEntity(minion);
		} else {
			LOGGER.error("Minion with this id doesn`t exist!");
		}
	}

	@Override
	public void deleteMinion(int id) {
		minionDao.deleteEntity(id);
	}

	@Override
	public Minion getMinionById(int id) {
		return minionDao.getEntityById(id);
	}

	@Override
	public List<Minion> getAllMinions() {
		return minionDao.getAllEntities();
	}
	
	private Minion createRandomMinion() {
		int min = getAllMinions().size();
		int max = Integer.MAX_VALUE;
		int minionId = random.nextInt((max - min) + 1) + min;
		return new Minion(minionId, "minion" + minionId, getRandomAge(), getRandomTown());
	}

	private Town getRandomTown() {
		int randomTownIndex = random.nextInt(townDao.getAllEntities().size());
		return townDao.getAllEntities().get(randomTownIndex);
	}

	private short getRandomAge() {
		return (short) random.nextInt(Short.MAX_VALUE);
	}

}
