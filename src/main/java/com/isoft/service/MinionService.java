package com.isoft.service;

import java.util.List;

import com.isoft.entity.Minion;

/**
 * Interface defining the MinionServiceImpl methods
 * @author Deyan Georgiev
 * @see MinionServiceImpl
 */
public interface MinionService {
	/**
	 * Method for saving a random minion in the database.
	 */
	void saveMinion();

	/**
	 * Method for updating a minion in the database.
	 * 
	 * @param minion
	 */
	void updateMinion(Minion minion);

	/**
	 * Method for deleting a minion in the database.
	 * 
	 * @param id
	 */
	void deleteMinion(int id);

	/**
	 * Method for getting a minion by its id from the database.
	 * 
	 * @param id
	 * @return the town
	 */
	Minion getMinionById(int id);

	/**
	 * Method for getting all minions from the database.
	 * 
	 * @return
	 */
	List<Minion> getAllMinions();
}
