package com.isoft.entity;

/**
 * Abstract class which contains the id property of all the entities.
 * @author Deyan Georgiev
 */
public abstract class BaseEntity {

    private int id;

    public BaseEntity(int id) {
    	this.id = id;
    }

    /**
     * @return the id
     */
    public int getId() {
        return this.id;
    }
}