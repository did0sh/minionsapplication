package com.isoft.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.isoft.connector.DatabaseConnector;
import com.isoft.entity.Evilness;
import com.isoft.entity.SuperPower;
import com.isoft.entity.Villain;

/**
 * Implementation class for CRUD operations referencing Villain.
 * 
 * @author Deyan Georgiev
 */
public class VillainDao implements EntityDao<Villain> {

	private static final Logger LOGGER = Logger.getLogger(VillainDao.class);

	@Override
	public void saveEntity(Villain entity) {
		Connection connection = DatabaseConnector.getConnection();

		if(connection != null) {
			try (PreparedStatement stmt = connection
					.prepareStatement("INSERT INTO villains(id, name, evilness, superpower) values (?, ?, ?, ?)")) {
				stmt.setInt(1, entity.getId());
				stmt.setString(2, entity.getName());
				stmt.setString(3, entity.getEvilness().name());
				stmt.setObject(4, entity.getSuperpower().name());

				stmt.executeUpdate();
				LOGGER.info("Successfully saved the villain entity!");
				connection.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			} catch (NullPointerException npe) {
				LOGGER.error("Villain entity should not be null!");
			}
		}
	}

	@Override
	public void updateEntity(Villain entity) {
		Connection connection = DatabaseConnector.getConnection();

		if(connection != null) {
			try (PreparedStatement stmt = connection
					.prepareStatement("UPDATE villains set name=?, evilness=?, superpower=? WHERE id=?")) {
				stmt.setString(1, entity.getName());
				stmt.setString(2, entity.getEvilness().name());
				stmt.setObject(3, entity.getSuperpower().name());
				stmt.setInt(4, entity.getId());

				stmt.executeUpdate();
				LOGGER.info("Successfully updated the villain entity!");
				connection.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}  catch (NullPointerException npe) {
				LOGGER.error("Villain entity should not be null!");
			}
		}
	}

	@Override
	public void deleteEntity (int id) {
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement selectStmt = connection.prepareStatement("SELECT * FROM villains WHERE id=?")){
				selectStmt.setInt(1, id);
				
				try (ResultSet rs = selectStmt.executeQuery()){
					if(rs.next()) {
						try (PreparedStatement stmt = connection.prepareStatement("DELETE FROM villains WHERE id=?")) {
							stmt.setInt(1, id);
							stmt.executeUpdate();
							LOGGER.info("Successfully deleted the villain entity!");
						}
					} else {
						LOGGER.info("Villain with this id doesn`t exist!");
					}
				}
				connection.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		}
	}

	@Override
	public Villain getEntityById(int id) {
		int villainId = 0;
		String villainName;
		Evilness villainEvilness;
		SuperPower villainSuperpower;
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM villains WHERE id=?")) {
				stmt.setInt(1, id);
				try (ResultSet rs = stmt.executeQuery()) {
					if (rs.next()) {
						villainId = rs.getInt("id");
						villainName = rs.getString("name");
						villainEvilness = Enum.valueOf(Evilness.class, rs.getString("evilness"));
						villainSuperpower = Enum.valueOf(SuperPower.class, rs.getString("superpower"));
						LOGGER.info("Successfully returned the villain entity!");
						return new Villain(villainId, villainName, villainEvilness, villainSuperpower);
					} else {
						LOGGER.error("Villain with this id doesn`t exist!");
					}
					connection.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		}
		return null;
	}

	@Override
	public List<Villain> getAllEntities() {
		int villainId;
		String villainName;
		Evilness villainEvilness;
		SuperPower villainSuperpower;
		List<Villain> allVillains = new ArrayList<>();
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM villains")) {
				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						villainId = rs.getInt("id");
						villainName = rs.getString("name");
						villainEvilness = Enum.valueOf(Evilness.class, rs.getString("evilness"));
						villainSuperpower = Enum.valueOf(SuperPower.class, rs.getString("superpower"));
						allVillains.add(new Villain(villainId, villainName, villainEvilness, villainSuperpower));
					}
					connection.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}

			if (allVillains.isEmpty()) {
				LOGGER.info("There are no entities in the database!");
			} else {
				LOGGER.info("Successfully returned all the villain entities!");
			}
		}

		return allVillains;
	}
}
