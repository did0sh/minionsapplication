package com.isoft.entity;

/**
 * Villain class that represents the villains table in the database.
 * @author Deyan Georgiev
 */
public class Villain extends BaseEntity {
	
	private String name;
	private Evilness evilness;
	private SuperPower superpower;

	/**
	 * @param id
	 */
	public Villain(int id, String name, Evilness evilness, SuperPower superpower) {
		super(id);
		this.name = name;
		this.evilness = evilness;
		this.superpower = superpower;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the evilness
	 */
	public Evilness getEvilness() {
		return evilness;
	}

	/**
	 * @return the superpower
	 */
	public SuperPower getSuperpower() {
		return superpower;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param evilness the evilness to set
	 */
	public void setEvilness(Evilness evilness) {
		this.evilness = evilness;
	}

	/**
	 * @param superpower the superpower to set
	 */
	public void setSuperpower(SuperPower superpower) {
		this.superpower = superpower;
	}

	@Override
	public String toString() {
		return "Villain: [name=" + name + ", evilness=" + evilness + ", superpower=" + superpower + "]";
	}

}
