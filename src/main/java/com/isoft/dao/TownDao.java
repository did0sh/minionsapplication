package com.isoft.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.isoft.connector.DatabaseConnector;
import com.isoft.entity.Town;

/**
 * Implementation class for CRUD operations referencing Town.
 * @author Deyan Georgiev
 */
public class TownDao implements EntityDao<Town> {
	
	private static final Logger LOGGER = Logger.getLogger(TownDao.class);

	@Override
	public void saveEntity(Town entity) {
		Connection connection = DatabaseConnector.getConnection();

		if(connection != null) {
			try (PreparedStatement stmt = connection
					.prepareStatement("INSERT INTO towns(id, name, country) values (?, ?, ?)")) {
				stmt.setInt(1, entity.getId());
				stmt.setString(2, entity.getName());
				stmt.setString(3, entity.getCountry());

				stmt.executeUpdate();
				LOGGER.info("Successfully saved the town entity!");
				connection.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			} catch (NullPointerException npe) {
				LOGGER.error("Town entity should not be null!");
			}
		}
		
	}

	@Override
	public void updateEntity(Town entity) {
		Connection connection = DatabaseConnector.getConnection();

		if(connection != null) {
			try (PreparedStatement stmt = connection
					.prepareStatement("UPDATE towns set name=?, country=? WHERE id=?")) {
				stmt.setString(1, entity.getName());
				stmt.setString(2, entity.getCountry());
				stmt.setInt(3, entity.getId());

				stmt.executeUpdate();
				LOGGER.info("Successfully updated the town entity!");
				connection.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			} catch (NullPointerException npe) {
				LOGGER.error("Town entity should not be null!");
			}
		}
	}

	@Override
	public void deleteEntity(int id) {
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement selectStmt = connection.prepareStatement("SELECT * FROM towns WHERE id=?")){
				selectStmt.setInt(1, id);
				
				try (ResultSet rs = selectStmt.executeQuery()){
					if(rs.next()) {
						try (PreparedStatement stmt = connection.prepareStatement("DELETE FROM towns WHERE id=?")) {
							stmt.setInt(1, id);
							stmt.executeUpdate();
							LOGGER.info("Successfully deleted the town entity!");
						}
					} else {
						LOGGER.info("Town with this id doesn`t exist!");
					}
				}
				connection.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		}
	}

	@Override
	public Town getEntityById(int id) {
		int townId;
		String townName;
		String townCountry;
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM towns WHERE id=?")) {
				stmt.setInt(1, id);
				try (ResultSet rs = stmt.executeQuery()) {
					if (rs.next()) {
						townId = rs.getInt("id");
						townName = rs.getString("name");
						townCountry = rs.getString("country");
						LOGGER.info("Successfully returned the town entity!");
						return new Town(townId, townName, townCountry);
					} else {
						LOGGER.error("Town with this id doesn`t exist!");
					}
					connection.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		}
		return null;
	}

	@Override
	public List<Town> getAllEntities() {
		int townId;
		String townName;
		String townCountry;
		List<Town> allTowns = new ArrayList<>();
		Connection connection = DatabaseConnector.getConnection();

		if (connection != null) {
			try (PreparedStatement stmt = connection.prepareStatement("SELECT * FROM towns")) {
				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						townId = rs.getInt("id");
						townName = rs.getString("name");
						townCountry = rs.getString("country");
						allTowns.add(new Town(townId, townName, townCountry));
					}
					connection.close();
				}
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}

			if (allTowns.isEmpty()) {
				LOGGER.info("There are no entities in the database!");
			} else {
				LOGGER.info("Successfully returned all the town entities!");
			}
		}

		return allTowns;
	}

}
