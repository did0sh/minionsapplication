package com.isoft.entity;

/**
 * Town class that represents the towns table in the database.
 * @author Deyan Georgiev
 */
public class Town extends BaseEntity {
	
	private String name;
	private String country;
	
	public Town(int id, String name, String country) {
		super(id);
		this.name = name;
		this.country = country;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Town: [name=" + name + ", country=" + country + "]";
	}
}
